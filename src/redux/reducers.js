import Auth from './auth/reducer';
import AdminJoinRequest from './admin/joinRequests/reducer';

// Combine all reducers.
export default {
  Auth,
  AdminJoinRequest
};