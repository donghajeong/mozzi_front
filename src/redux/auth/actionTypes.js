export const POST_SIGN_IN_PENDING = 'POST_SIGN_IN_PENDING';
export const POST_SIGN_IN = 'POST_SIGN_IN';
export const POST_SIGN_IN_SUCCESS = 'POST_SIGN_IN_SUCCESS';
export const POST_SIGN_IN_FAILURE = 'POST_SIGN_IN_FAILURE';

export const SIGN_OUT_PENDING = 'SIGN_OUT_PENDING';
export const SIGN_OUT = 'SIGN_OUT';
export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS';
export const SIGN_OUT_FAILURE = 'SIGN_OUT_FAILURE';

export const CHECK_AUTHORIZATION = 'CHECK_AUTHORIZATION';
